# Pulse [Drupal 7](http://drupal.org/) custom module

The Pulse Drupal module allows you to easily implement user centre feedback from the service [Pulse](https://pulse.webbhapens.co.uk).

### Installation

Simply enable the module, enter your site pubic id and you're good to go.

### Usage

Visit the [Pulse website](https://pulse.webbhapens.co.uk) to identify poor performing pages, as well as good ones, find out why this is the case using real feedback from your use, then act on this to improve your site and monitor over time.

<hr>

by [Web Happens](https://www.webhappens.co.uk).
