;( function( $, window, document, undefined ) {

    var instanceId = 0;

    if ( ! $.WebHappens ) $.WebHappens = {};

    $.WebHappens.Pulse = function( el, options ) {
console.log(this);
        // This 'class'
        var base = this;

        // Increment the instance Id
        instanceId++;

        // DOM object
        base.el = el;

        // jQuery element cache object
        base.$ = {};
        base.$.el = $( el );

        // Feedback object
        base.feedback = {
            id: null,
            token: null
        };

        // API URL
        base.apiUrl = 'http://local.pulse.webhappens.co.uk/api/1/' + options.site_id + '/feedback';

        // HTML
        base.html = 
            '<div class="wh-pulse-sentiment">\
                <p class="wh-pulse-question">' + options.question + '</p>\
                <div class="wh-pulse-form-group">\
                    <a class="wh-pulse-button-positive" rel="nofollow" href="#positive">' + options.sentiment_positive_text + '</a>\
                    <a class="wh-pulse-button-negative" rel="nofollow" href="#negative">' + options.sentiment_negative_text + '</a>\
                </div>\
            </div>\
            <div class="wh-pulse-message">\
                <div class="wh-pulse-form-group">\
                    <label for="wh-pulse-message-box-' + instanceId + '">' + options.message_label + '</label>\
                    <textarea id="wh-pulse-message-box-' + instanceId + '" class="wh-pulse-message-box" placeholder="' + options.message_placeholder + '"></textarea>\
                </div>\
                <div class="wh-pulse-form-group">\
                    <button type="button" class="wh-pulse-button-send" name="submit-message">' + options.message_button_text + '</button>\
                </div>\
            </div>\
            <div class="wh-pulse-thanks">' + options.message_thanks_html + '</div>\
            <div class="wh-pulse-after-positive">' + options.after_positive_html + '</div>\
            <div class="wh-pulse-after-negative">' + options.after_negative_html + '</div>';


        // Init
        base._init = function()
        {
            // Add the HTML to the DIV
            base.$.el.html( base.html );
console.log(base.$);

            // Bootstrapify the markup
            if ( options.bootstrap ) base._bootstrapify();

            // Create references to key components
            base.$.sentiment = base.$.el.find( 'div.wh-pulse-sentiment' );
            base.$.afterPositive = base.$.el.find( 'div.wh-pulse-after-positive' );
            base.$.afterNegative = base.$.el.find( 'div.wh-pulse-after-negative' );
            base.$.message = base.$.el.find( 'div.wh-pulse-message' );
            base.$.messageTextarea = base.$.el.find( 'div.wh-pulse-message textarea' );
            base.$.thanks = base.$.el.find( 'div.wh-pulse-thanks' );
            base.$.positiveButton = base.$.el.find( 'a.wh-pulse-button-positive' );
            base.$.negativeButton = base.$.el.find( 'a.wh-pulse-button-negative' );
            base.$.sendButton = base.$.el.find( 'div.wh-pulse-message button' );

console.log(base.$);
            // Setup the event listeners
            base._eventListeners();
        };

        base._sendSentiment = function( sentiment )
        {
            var request = base._post(
                base.apiUrl,
                {
                    sentiment: sentiment,
                    page_title: options.page_title,
                    widget_name: base.$.el.data( 'name' ),
                    widget_label: base.$.el.data( 'label' )
                }
            );

            request
                .done( function( data )
                    {
                        base.feedback = data;

                        base.$.el.trigger( 'sentimentDone', [
                            {
                                'feedbackId': base.feedback.id,
                                'sentiment': sentiment
                            }
                        ] );
                    }
                )
                .fail( function( data ) 
                    {
                        // Rollback the UI
                        base.$.sentiment.show();
                        base.$.afterPositive.hide();
                        base.$.afterNegative.hide();
                        base.$.message.hide();
                        base.$.messageTextarea.blur();
                    }
                );

            // Update the UI
            base.$.sentiment.hide();

            if ( sentiment == 'positive' && options.after_positive_html != false )
            {
                base.$.afterPositive.show();
            }
            else if ( sentiment == 'negative' && options.after_negative_html != false )
            {
                base.$.afterNegative.show();
            }

            if ( options.message_box )
            {
                base.$.message.show();
                base.$.messageTextarea.focus();
            }
        };

        base._validateMessage = function()
        {
            var message = base.$.messageTextarea.val();

            if ( message.length > 0 && message.length < 501 )
            {
                return true;
            }

            return false;
        };

        base._sendMessage = function()
        {
            if ( base._validateMessage() )
            {
                if ( base.feedback.id )
                {
                    var request = base._patch(
                        base.apiUrl + '/' + base.feedback.id,
                        {
                            message: base.$.messageTextarea.val(),
                            token: base.feedback.token
                        }
                    );

                    request
                        .done( function( data )
                            {
                                base.$.el.trigger( 'messageDone', [
                                    {
                                        'feedbackId': base.feedback.id
                                    }
                                ] );

                                base.feedback.id = null;
                                base.feedback.token = null;
                            }
                        )
                        .fail( function()
                            {
                                // Rollback the UI
                                base.$.message.show();
                                base.$.thanks.hide();
                            }
                        );

                    // Update the UI
                    base.$.message.hide();
                    base.$.thanks.show();
                }
                else
                {
                    alert( 'We\'re still waiting for your last request to process, please wait a few moments and try again.' );
                }
            }
            else
            {
                alert( 'Your message must be between 1 and 500 characters in length' );
            }
        };

        base._post = function( url, data )
        {
            return base._send( 'POST', url, data );
        };

        base._patch = function( url, data )
        {
            return base._send( 'PATCH', url, data );
        };

        base._send = function( type, url, data )
        {
            var request = $.ajax(
                {
                    url: url,
                    type: type,
                    dataType: 'json',
                    data: data
                }
            )
            .fail( function( jqXHR )
                {
                    console.log( jqXHR );

                    switch ( jqXHR.status )
                    {
                        case 422:
                            // Validation errors
                            var errorArr = [];

                            $.each( jqXHR.responseJSON, function( key, value )
                            {
                                errorArr.push( value );
                            } );

                            alert( errorArr.join( "\n" ) );

                            break;

                        case 403:
                            // Forbidden errors
                            alert( 'You do not have permission to do this.' );
                            
                            break;

                        default:
                            // General error
                            alert( 'An error has occurred. Please try again.' );

                            break;
                    }
                }
            );

            return request;
        };

        // Modify the markup for Bootstrap
        base._bootstrapify = function()
        {
            base.$.el.find( '.wh-pulse-button-positive, .wh-pulse-button-negative' ).addClass( 'btn btn-default' );
            base.$.el.find( '.wh-pulse-button-send' ).addClass( 'btn btn-primary' );
            base.$.el.find( '.wh-pulse-form-group' ).addClass( 'form-group' );
            base.$.el.find( '.wh-pulse-message-box' ).addClass( 'form-control' );
            base.$.el.find( '.wh-pulse-thanks' ).addClass( 'alert alert-success' );
        };

        base._eventListeners = function()
        {
            base.$.positiveButton.bind( 'click', function( event )
            {
                base._sendSentiment( 'positive' );
                event.preventDefault();
            } );

            base.$.negativeButton.bind( 'click', function( event )
            {
                base._sendSentiment( 'negative' );
                event.preventDefault();
            } );

            base.$.sendButton.bind( 'click', function()
            {
                base._sendMessage();
            } );
        };

        base._init();

    };


    // Create jQuery plugin wrapper
    $.fn.WebHappens_Pulse = function( options )
    {
        options = $.extend(
            {
                site_id: null,
                page_title: document.title,
                bootstrap: false,
                
                question: 'Did you find this useful?',
                sentiment_positive_text: 'Yes',
                sentiment_negative_text: 'No',
                message_box: true,
                message_label: 'Please provide some feedback',
                message_placeholder: 'Type your message here',
                message_button_text: 'Send',
                message_thanks_html: '<p>Thank you!</p>',
                after_positive_html: false,
                after_negative_html: false
            },
            $.fn.WebHappens_Pulse.options,
            options
        );

        return this.each( function()
        {
            new $.WebHappens.Pulse( this, options );
        } );
    };


}) ( jQuery, window, document );